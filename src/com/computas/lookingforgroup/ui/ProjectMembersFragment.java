package com.computas.lookingforgroup.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

public class ProjectMembersFragment extends Fragment{
	
	Uri mProjectUri;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final Intent intent = BaseActivity.fragmentArgumentsToIntent(getArguments());
		mProjectUri = intent.getData();
		Log.e("ProjectMembersFragment", "Uri: " + mProjectUri);
	}
}
