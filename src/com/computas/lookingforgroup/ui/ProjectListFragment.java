package com.computas.lookingforgroup.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.computas.lookingforgroup.Constants;
import com.computas.lookingforgroup.R;
import com.computas.lookingforgroup.model.Profile;
import com.computas.lookingforgroup.model.Project;

public class ProjectListFragment extends ListFragment{
	
	private GroupAdapter mGroupAdapter;
	private List<Project> mGroupList = new ArrayList<Project>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Project g1 = new Project();
		g1.id = "1";
		g1.name = "Supergroup for IT";
		g1.description = "Supergroup 1";
		Project g2 = new Project();
		g2.id = "1";
		g2.name = "Android programming 101";
		Project g3 = new Project();
		g3.id = "1";
		g3.name = "Master Project: Why mobile is fun?";
		Project g4 = new Project();
		g4.id = "1";
		g4.name = "Study group for MATH 603";
		Project g5 = new Project();
		g5.id = "1";
		g5.name = "Computas Lovers (PS: Full)";
		
		mGroupList.add(g1);
		mGroupList.add(g2);
		mGroupList.add(g3);
		mGroupList.add(g4);
		mGroupList.add(g5);
		
		mGroupAdapter = new GroupAdapter(getActivity());
	
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getListView().setBackgroundColor(Color.RED);
		setListAdapter(mGroupAdapter);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		//Load data for the list here
		new GetProjectTask().execute("projects/");
	}
	
	private class GetProjectTask extends AsyncTask<String, Void, Project[]> {

		@Override
		protected Project[] doInBackground(String... params) {
			try {
				String url = Constants.BASE_URL + params[0];
				
				// Create a new RestTemplate instance
				RestTemplate restTemplate = new RestTemplate();
				// Add the Gson message converter
				restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
				
				Log.e("RestCall", "Calling:" + url);
				Project[] projects = restTemplate.getForObject(url, Project[].class);

				return projects;
			} catch (Exception e) {
				Log.e("RestCall", "Get projects failed: " + e.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Project[] projects) {
			if (projects != null) {
				Log.e("RestCall", "Got: " + projects.length);
				mGroupList.clear();
				mGroupList.addAll(Arrays.asList(projects));
				mGroupAdapter.notifyDataSetChanged();
			}
		}
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		//Show details for the clicked group
		Intent intent = new Intent(getActivity(), ProjectDetailsActivity.class);
		intent.setData(Uri.parse("groups/" + mGroupList.get(position).id));
		startActivity(intent);
	}
	
	private class GroupAdapter extends ArrayAdapter<Project> {

		public GroupAdapter(Context context) {
			super(context, R.layout.list_item_group, R.id.group_name, mGroupList);
		}

//		@Override
//		public View getView(int position, View view, ViewGroup parent) {
//			if (view == null) {
//				LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
//						Context.LAYOUT_INFLATER_SERVICE);
//				view = vi.inflate(R.layout.list_item_main_menu, null);
//			}
//
//			final ActionBarMenuItem item = getItem(position);
//			TextView menuText = (TextView) view.findViewById(R.id.menu_text);
//			menuText.setText(item.title);
//			return view;
//		}
	}
}
