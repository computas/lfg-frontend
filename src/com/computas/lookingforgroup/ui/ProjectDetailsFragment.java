package com.computas.lookingforgroup.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost.TabSpec;

public class ProjectDetailsFragment extends Fragment{

	private FragmentTabHost mTabHost;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mTabHost = new FragmentTabHost(getActivity());
		mTabHost.setup(getActivity(), getChildFragmentManager());

		// Own activities tab
		TabSpec spec1 = mTabHost.newTabSpec("tab1").setIndicator("Beskrivelse");
		mTabHost.addTab(spec1, ProjectDescriptionFragment.class, getArguments());
		
		// Own activities tab
		TabSpec spec2 = mTabHost.newTabSpec("tab2").setIndicator("Medlemmer");
		mTabHost.addTab(spec2, ProjectMembersFragment.class, getArguments());
		
		// Own activities tab
		TabSpec spec3 = mTabHost.newTabSpec("tab3").setIndicator("Skills");
		mTabHost.addTab(spec3, ProjectSkillsFragment.class, getArguments());

		return mTabHost;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mTabHost = null;
	}
}
