package com.computas.lookingforgroup.ui;

import java.util.Arrays;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.computas.lookingforgroup.Constants;
import com.computas.lookingforgroup.R;
import com.computas.lookingforgroup.model.Project;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

public class ProjectDescriptionFragment extends Fragment{
	
	Uri mProjectUri;
	Project g1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		g1 = new Project();
		g1.id = "1";
		g1.name = "Supergroup for IT";
		g1.description = "Supergroup 1";
		
		final Intent intent = BaseActivity.fragmentArgumentsToIntent(getArguments());
		mProjectUri = intent.getData();
		Log.e("ProjectDescription", "Uri: " + mProjectUri);
		
		
		
		
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View root= inflater.inflate(R.layout.description, container);

		TextView text = (TextView)root.findViewById(R.id.editText1);
		text.setText("tst");

		return root;
    }

	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getView().setBackgroundColor(Color.RED);
		/*setListAdapter(mGroupAdapter);*/
		
	}
	
	@Override
	public void onResume() {
		super.onResume();

		//Load data for the list here
		new GetProjectTask().execute(mProjectUri.toString());
	}
	
	private class GetProjectTask extends AsyncTask<String, Void, Project> {

		@Override
		protected Project doInBackground(String... params) {
			try {
				String url = Constants.BASE_URL + mProjectUri;
				
				// Create a new RestTemplate instance
				RestTemplate restTemplate = new RestTemplate();
				// Add the Gson message converter
				restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
				
				Log.e("RestCall", "Calling:" + url);
				Project project = restTemplate.getForObject(url, Project.class);

				return project;
			} catch (Exception e) {
				Log.e("RestCall", "Get project failed: " + e.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Project project) {
			if (project != null) {
				Log.e("ProjectDescription", "Uri: " + project.description);
				
			}
		}
	}

}
